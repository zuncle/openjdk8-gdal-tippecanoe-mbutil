FROM zuncle/gdal-tippecanoe-mbutil:latest

MAINTAINER Xavier Thomas <xavier.thomas@pixime.fr>

ENV JAVA_HOME=/usr/lib/jvm/default-jvm \
    JAVA_VERSION=8u171 \
    JAVA_ALPINE_VERSION=8.242.08-r0

ENV PATH=$PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin \
    LANG=C.UTF-8

RUN apk add --no-cache openjdk8="$JAVA_ALPINE_VERSION"
